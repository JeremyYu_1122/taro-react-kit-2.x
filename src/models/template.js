import { xxxxxxApi } from '@/services/template';


export default {
  namespace: 'template',

  state: {
    yyyy: false, // 弹出提示框强制用户授权
  },

  effects: {
    *xxxxxx({ payload }, { call, put }) {
      const response = yield call(xxxxxxApi, payload);

      // * 如果有httpStatusCode，说明接口异常，该属性的值是4xx（401 除外）或5xx
      if (response && response.httpStatusCode) {
        // xxxx，如果需要有其它操作可以写在这里
        throw new Error(JSON.stringify(response));
      }

      yield put({
        type: '_xxxxxx',
        payload: response,
      });

      return response;
    },
  },

  reducers: {
    _xxxxxx(state, { payload }) {
      return {
        ...state,
        yyyy: payload,
      }
    },
  }
};
