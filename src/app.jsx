import Taro, { Component } from '@tarojs/taro';
import { Provider } from '@tarojs/redux';

import Index from './pages/index';

import dva from './utils/dva';
import models from './models';

import "./taroUiTheme/customTheme.scss"; // 自定义主题
import './app.less';


// 如果需要在 h5 环境中开启 React Devtools
// 取消以下注释：
// if (process.env.NODE_ENV !== 'production' && process.env.TARO_ENV === 'h5')  {
//   require('nerv-devtools')
// }

const dvaApp = dva.createApp({
  initialState: {},
  models: models,
});
const store = dvaApp.getStore();

class _App extends Component {

  config = {
    pages: preval`
      module.exports=(function() {
        return [
          // add pages
          'pages/index/index',
          'pages/mine/mine',
        ];
      })()
    `,

    subpackages: [
      
    ],

    window: preval`
      module.exports=(function() {        
        return {
          backgroundTextStyle: 'light',
          navigationBarBackgroundColor: '#fff',
          navigationBarTitleText: '首页',
          navigationBarTextStyle: 'black',
          backgroundColor: '#ff8f62',
        };
      })()
    `,

    plugins: {
      // "coral-adv": {
      //   "version": "1.0.7",
      //   "provider": "wx0e203209e27b1e66"
      // },
      // "mini-activity": {
      //   "version": "2.0.3", // 注意：这里请改为当前最新版本
      //   "provider": "wx4645f4f355c0521a"
      // },
    },

    tabBar: preval`
      module.exports=(function() {
        return {
          "color": "#333",
          "selectedColor": "#ff9074",
          "backgroundColor": "#fff",
          "list": [
            {
              "pagePath": "pages/index/index",
              "text": "首页",
              "iconPath": "assets/tabbar/icon_leaderboard_off.png",
              "selectedIconPath": "assets/tabbar/icon_leaderboard_on.png"
            },
            {
              "pagePath": "pages/mine/mine",
              "text": "我的",
              "iconPath": "assets/tabbar/icon_mine_off.png",
              "selectedIconPath": "assets/tabbar/icon_mine_on.png"
            }
          ]
        };
      })()
    `,

    // 需要跳转的小程序列表
    navigateToMiniProgramAppIdList: preval`
      module.exports=(function() {
        return []
      })()
    `,
  }

  // 定义全局数据 globalData
  // 写清楚哪里会设置 set，哪里获取数据 get
  // 通过直接改变值的方式处理，例如 getApp().globalData.userInfo = userInfo;
  // 固定定义 userInfo: {}, 通过用 userInfo.userId 判断是否有用户信息
  // token 放在 localStorage （管理后台是cache，本质也是 localStorage）
  globalData = {
    userInfo: {},

    coopenParams: {}, // 存放开屏页面的参数（当从开屏页面跳转到 tabBar 的页面时，this.$router.params 会丢失，所以只能放在这个位置存放）

    apiPrefix: '', // 后台接口前缀，从后台配置文件中读取的值放在两个地方，一个是 globalData 里面，另一个是在 systemSetting models中
  }

  // 临时传参数据 tempGlobalData
  // 用于 switchTab 页面或者其他的需要临时存放在全局的数据
  tempGlobalData = {

  }

  // 进入小程序后调一次
  enterApp = () => {
    console.log('进入小程序');
  }

  componentDidMount() {
    const updateManager = Taro.getUpdateManager()
    // console.log('updateManager', updateManager)

    updateManager.onCheckForUpdate(function (res) {
      // 请求完新版本信息的回调
      console.log("hasUpdate", res.hasUpdate)
    })

    updateManager.onUpdateReady(function () {
      Taro.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        confirmColor: '#006EFC',
        success: (res) => {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        }
      })
    })
    updateManager.onUpdateFailed(function () {
      // 新版本下载失败
    })

    this.enterApp();
  }

  componentDidShow() {
    console.log('app-componentDidShow');
  }

  componentDidHide() {
    console.log('app-componentDidHide');
  }

  componentDidCatchError(error) {
    console.log('componentDidCatchError', error);

  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return (
      <Provider store={store}>
        <Index />
      </Provider>
    )
  }
}

Taro.render(<_App />, document.getElementById('app'))
