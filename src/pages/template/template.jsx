// 第三方库
import Taro, { Component } from '@tarojs/taro';
import { View, Button, Text } from '@tarojs/components';
import { AtToast, AtButton, AtMessage } from 'taro-ui';
import { connect } from '@tarojs/redux';

// 自定义组件


// 自定义工具库、工具函数
// import * as appConfig from '@/utils/appConfig';
// import * as cache from '@/utils/toolbox/cache';

// 图片
import logo from '@/assets/logo.png';

// 样式
import './template.less';


// TODO: 全局变量，少的写这里，多且长，超过15行增加这个文件constant.js

@connect(({ template, loading }) => ({
  template,
  isPageLoading: loading.effects['template/xxxxxx']
    || loading.effects['template/xxxxxx2'],
    // || loading.effects['test/querySameName'] 在这里追加 page-loading
}))
class Template extends Component {
  config = {
    navigationBarTitleText: 'TODO: 记得修改标题',
    // enablePullDownRefresh: true, // 允许用户下拉刷新
    // enableShareAppMessage: true, // 允许用户分享页面
  }

  // static 都写在这个位置
  // static options = {}

  // defaultProps 都写在这个位置
  // static defaultProps = {}

  state = {
  }

  constructor(props) {
    super(props);

  }

  componentWillReceiveProps (nextProps) {
    console.log(this.props, nextProps)
  }

  componentWillMount () {}

  async componentDidMount() {
    // this.fetchInitData(); //  把页面所有初始化数据的接口调用都放在这个函数里面
  }

  componentWillUnmount () {}

  componentDidShow () {}

  componentDidHide () {}

  //  监听用户下拉刷新事件
  // onPullDownRefresh() {
  //   // this.fetchInitData();  把页面所有初始化数据的接口调用都放在这个函数里面
  // }

  // 监听用户点击页面内转发按钮（Button 组件 openType='share'）或右上角菜单“转发”按钮的行为，并自定义转发内容
  // onShareAppMessage() {
  //   return {
  //     title: '自定义转发标题',
  //     path: '/page/user?id=123'
  //   }
  // }

  fetchInitData = () => {
    this.props.dispatch({
      type: 'template/xxxxxx',
      payload: {
        
      }
    }).then((res) => {
      console.log('fetchInitData res', res);
    })
  }

  render () {
    return (
      <View>
        <AtToast isOpened={this.props.isPageLoading} text="正在加载..." status="loading" duration={0} hasMask={true}></AtToast>
        <View>Template</View>
      </View>
    )
  }
}

export default Template;
