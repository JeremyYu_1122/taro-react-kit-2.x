// 第三方库
import Taro, { Component } from "@tarojs/taro";
import { View, Button, Text, Image } from "@tarojs/components";
import { AtToast, AtInput } from "taro-ui";
import { connect } from "@tarojs/redux";

// 自定义组件


// 自定义工具库、工具函数
import * as appConfig from "@/utils/appConfig";
import * as cache from "@/utils/toolbox/cache";
import * as utils from "@/utils/utils";

// 图片

// 样式
import "./index.less";

@connect(({ template, loading }) => ({
  template,
  isPageLoading:
    loading.effects["template/xxxxxx"] 
  // || loading.effects['test/querySameName'] 在这里追加 page-loading
}))
class Index extends Component {
  config = {
    navigationBarTitleText: "首页",
  };


  state = {
    
  };

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props, nextProps);
  }

  componentWillMount() { }

  async componentDidMount() {
    await this.fetchInitData();

    
  }

  componentWillUnmount() { }


  componentDidShow() {

  }

  componentDidHide() {
   
  }

  // onShareAppMessage() {
  //   return {
  //     title: '分享页面',
  //     path: '/pages/index/index',
  //   };
  // }


  fetchInitData = () => {
    this.props.dispatch({
      type: 'template/xxxxxx',
      payload: {
        
      }
    }).then((res) => {
      console.log('fetchInitData res', res);
    })
  }


  render() {
    return (
      <View>
        <AtToast
          isOpened={this.props.isPageLoading}
          text="正在加载..."
          status="loading"
          duration={0}
          hasMask={true}
        ></AtToast>


        <View className="flex-b-cc container">
          <View className="flex-b-cc">Hello World！This is Chatlasb!</View>
        </View>
      </View>
    );
  }
}

export default Index;
