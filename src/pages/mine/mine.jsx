// 第三方库
import Taro, { Component } from "@tarojs/taro";
import { View, Button, Text, Image } from "@tarojs/components";
import { AtToast, AtInput } from "taro-ui";
import { connect } from "@tarojs/redux";

// 自定义组件


// 自定义工具库、工具函数


// 图片

// 样式
import "./mine.less";

class Mine extends Component {
  config = {
    navigationBarTitleText: "我的",
  };


  state = {
    
  };

  constructor(props) {
    super(props);
  }

  componentWillReceiveProps(nextProps) {
    console.log(this.props, nextProps);
  }

  componentWillMount() { }

  async componentDidMount() {
    // await this.fetchAppVersion(); // 请求后台配置文件

    
  }

  componentWillUnmount() { }


  componentDidShow() {

  }

  componentDidHide() {
   
  }

  onShareAppMessage() {
    return {
      title: '分享页面',
      path: '/pages/mine/mine',
    };
  }

  render() {
    

    return (
      <View>
        <View className="container">
          个人中心
        </View>
      </View>
    );
  }
}

export default Mine;
