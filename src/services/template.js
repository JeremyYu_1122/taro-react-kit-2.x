/*
 * @Author: your name
 * @Date: 2020-04-21 14:44:59
 * @LastEditTime: 2020-04-21 14:45:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /taro-kit/src/services/template.js
 */
import { stringify } from 'qs';
import * as appConfig from '../utils/appConfig';
import request from '../utils/request';

export async function xxxxxxApi (params) {
  return request(`${appConfig.apiPrefix}/mock/71/api/tmg_addon/web_qy_session/department/list?${stringify(params)}`);
}

export async function xxxxxxApi2(params) {
  return request(`${appConfig.apiPrefix}/api/login`, {
    method: 'POST',
    body: {
      ...params,
    },
  });
}

// 不展示错误
export async function xxxxxxApi3(params) {
  return request(`${appConfig.apiPrefix}/api/extension/kitty?${stringify(params)}`, {}, {
    isShowErrorToast: false,
  });
}