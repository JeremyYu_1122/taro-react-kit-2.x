import Taro from '@tarojs/taro';
import lodash from 'lodash';

import * as cache from '@/utils/toolbox/cache';
import * as appConfig from '@/utils/appConfig';

/**
 * 重塑options
 * @param {Object} defaultOptions 默认options
 * @param {Object} options 用户配置的options
 */
const reshapeOptions = (defaultOptions, options) => {
  const newOptions = { ...defaultOptions, ...options };

  const token = cache.get('token');
  const headers = {
    Authorization: `token ${token}`,
    // Authorization: `token 073E7C05F309B703AB15E921EB9FBEA5`,

    // Authorization: `token FE6BCC6D9318B2191BB41B3B8CD5BFF1`,
    ...newOptions.headers,
  };

  newOptions.headers = headers;

  if (newOptions.method === 'POST' || newOptions.method === 'PUT') {
    // FIXME: 在小程序中暂无设置 Accept 值
    // newOptions.headers.Accept = 'application/json';

    // FIXME: 在小程序中 Content-Type 默认为 application/json，所以不需要额外设置
    // if (newOptions.type === 'json') {
    //   // newOptions.headers['Content-Type'] = 'application/json; charset=utf-8';
    //   // newOptions.body = JSON.stringify(newOptions.body);
    // }
    if (newOptions.type === 'formData') {
      // FIXME: 如果是以formData形式传输，在小程序里面需要设置 Content-Type
      newOptions.headers['Content-Type'] = 'application/x-www-form-urlencoded';
      // newOptions.body = toFormData(newOptions.body); // Taro 要使用 data 进行传参
    }
  }

  // FIXME: 最终发送给服务器的数据是 String 类型，如果传入的 data 不是 String 类型，会被转换成 String 。转换规则如下：
  //   对于 GET 方法的数据，会将数据转换成 query，string（encodeURIComponent(k)=encodeURIComponent(v)&encodeURIComponent(k)=encodeURIComponent(v)...）
  //   对于 POST 方法且 header['content-type'] 为 application/json 的数据，会对数据进行 JSON 序列化
  //   对于 POST 方法且 header['content-type'] 为 application/x-www-form-urlencoded 的数据，会将数据转换成 query string，（encodeURIComponent(k)=encodeURIComponent(v)&encodeURIComponent(k)=encodeURIComponent(v)...）
  newOptions.data = newOptions.body;

  return newOptions;
};

/**
 * 为了避免提审时接口使用的是生产环境的接口，但是这时生产环境可能还没更新，所以，为了避免这种情况，小程序的接口全部统一从后台的配置文件中获取
 * @tutorial get-config-from-server
 * @param {String} url services 传递进来的url
 */
const urlFilter = (url) => {
  let newUrl = '';
  const apiPrefix = (Taro.getApp() && Taro.getApp().globalData && Taro.getApp().globalData.apiPrefix) || appConfig.apiPrefix;

  console.log(`apiPrefix :`, apiPrefix);
  console.log(`url :`, url);

  // 如果传递进来的 url 中包含 'systemSetting/appVersion' 或者 warninglimit 说明是在请求后台的配置文件，则不做过滤
  // 否则，要看 appConfig.apiPrefix === 'useConfigJson'，如果是则拿 globalData 的数据（已经从接口中获取并保存在 globalData 中）
  if (url.includes('systemSetting/appVersion') || url.includes('warninglimit')) {
    newUrl = url;
  } else {
    newUrl = appConfig.apiPrefix === 'useConfigJson' ? url.replace('useConfigJson', apiPrefix) : url;
  }

  console.log(`newUrl :`, newUrl);
  return newUrl;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 *      type：发送类型，json 或者 formData
 * @param  {object} [toggle] 一些额外的开关选项
 *      responseType: '', 制定请求的返回值，  // FIXME: 小程序的 responseType 跟 cms 的不同
 *          text  -- 响应的数据为文本
 *          响应的数据为文本 -- 响应的数据为 ArrayBuffer
 *          默认 -- 空
 *      isShowErrorToast: true, 标志，当 status >= 400 时，是否采用统一的 toast 提示用户
 * @return {object}           An object containing either "data" or "err"
 */
export default function request (
  url,
  options = {},
  toggles = {
    responseType: '',
    isShowErrorToast: true // * 用于判断是否当 status >= 400 时的请求以通用toast的形式展示
  }
) {
  const defaultOptions = {
    // credentials: 'include', // * 如果是使用 cookie，则需要开启此开关
    // timeout: 10000, // 设置超时时间， 默认最大是 60s
  };

  const {
    isShowErrorToast
  } = toggles;

  const newOptions = reshapeOptions(defaultOptions, options);

  const requestParams = {
    url: urlFilter(url),
    ...newOptions,
    header: (url.includes('/api/marketing') || url.includes('/api/ad')) ? lodash.omit(newOptions.headers, ['Authorization']) : newOptions.headers,
    timeout: 20 * 1000,
  };

  return Taro.request(requestParams)
    .then((response) => {
      let {
        statusCode,
        data
      } = response

      if (statusCode === 200) {
        return response.data;
      }

      console.log(`
      ------------- start ------------
      接口：${newOptions.method || 'GET'} ${urlFilter(url)}
      ${newOptions.headers.Authorization}
      参数：${JSON.stringify(newOptions.body)}
      状态：${statusCode}
      返回：${JSON.stringify(data)}
      -------------- end -----------
      `);

      // 当接口返回 401 时，说明 token 过期、接口访问失败，需要登录
      // 当遇到401 时，直接抛出一个错误
      if (statusCode === 401) {
        throw new Error('statusCode-401'); // 抛出错误只会，下面的代码就不会执行了
      }
      
      // 如果是 400、4xx（401 除外）、5xx，则根据是否需要弹出提示框判断是否提示用户
      if (statusCode >= 400 && isShowErrorToast) {
        let { msg = '', code = '' } = data;

        // 拦截用户未绑定的情况，不直接给用户报错
        if (statusCode != 404) {
          Taro.showToast({
            title: msg,
            icon: 'none',
            mask: true,
          });
        }
      }

      return {
        httpStatusCode: statusCode, // * 如果有httpStatusCode，说明接口异常，该属性的值是4xx（401 除外）或5xx
        response
      };
    })
    .catch((err) => {
      // 因为目前采用的是 Promise.all 的方式捕抓 401 的错误，所以在整个 request、models 中的异步函数都不能写 try catch，除非已经确定不会进入到 401 的捕抓当中
      // ps：可能 request 返回的不纯是 promise 对象，这里拦截的是 request 的错误，如果在 then 中抛出异常，是不会拦截的
      console.log('requst-catch', err);
      // 响应超时
      if (err && err.errMsg && err.errMsg === 'request:fail timeout') {
        // 上报错误
        wx && wx.aldstat.sendEvent('request_fail_timeout', {
          userId: `${cache.get('userId')}`,
          url: url,
        });

        Taro.showModal({
          title: '提示',
          content: '网络异常，请退出微信，检查网络后重新进入小程序',
          showCancel: false,
          success: (res) => {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        });
      } else {
        throw err;
      }
    });
}
