import Taro from '@tarojs/taro';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
import utc from 'dayjs/plugin/utc';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import lodash from 'lodash';
import * as cache from '@/utils/toolbox/cache';
import * as appConfig from '@/utils/appConfig';

dayjs.extend(duration);
dayjs.extend(utc)
dayjs.extend(customParseFormat)

/**
 * 返回当前时间与目标时间零点的时间间隔
 * @param {number} target 目前时间，设定是当前时间的多少天后
 * @param {string} returnType 返回值的类型 (dayjs Duration 的类型)
 *    http://image.talkmoney.cn/img/20200619114118.png
 */
export const getNowAndTargetTimeDuration = (target, returnType = 'hours') => {
  let now = dayjs();
  let targetWeeHours = dayjs().add(target, 'd').hour(0).minute(0).second(0).millisecond(0);
  let duration = dayjs.duration(targetWeeHours - now)[returnType]();

  return duration;
};

/**
 * 将 utc时间转成是北京时间（增加8个小时，北京时间比utc时间多八个时区）
 */
export const beijingTime = () => {
  return dayjs().utc().utcOffset(8);
};

// dayjs('01:00', "HH:mm").utc().utcOffset(8).valueOf()
export const getParseFormatBeijingTime = (time, format) => {
  return dayjs(time, format).utc().utcOffset(8).valueOf();
};

export const getDayjsFormat = (time = undefined, format = 'MM-DD') => {
  return dayjs(time).format(format);
};


/*计算日期差*/
export function diffTime(diffSeconds) {
  // let dif = dayjs(end).diff(dayjs(), 'seconds'); // 与终点差多少秒
  let days = Math.floor(diffSeconds / (24 * 60 * 60)); // 将秒换算成剩余天
  let hours = Math.floor(diffSeconds / 60 / 60) - days * 24; // 将秒换算成剩余小时
  let minutes = Math.floor(diffSeconds / 60) - hours * 60 - days * 24 * 60; // 将秒换算成剩余分钟
  let seconds = diffSeconds % 60;

  return {
    days,
    hours,
    minutes,
    seconds,
  }
}

/**
 * 将两种 router 参数合并成一个
 * @param {Object} routerParams this.$router.params
 */
export function getFuseRouterParams(routerParams) {
  return {
    ...routerParams,
    ...Taro.getApp().globalData.coopenParams,
  }
}

export const transImgList = async (list) => {
  let imgTransPromise = list.map(item => {
    const token = cache.get('token');
    return Taro.uploadFile({
      url: `${appConfig.apiPrefix}/api/module/qiniu/one`,
      filePath: item.url,
      name: 'file',
      header: {
        Authorization: `token ${token}`
      },
      formData: {
        key: item.url
      },
    }).then ((res) => {
      let newImg = '';
      try {
        newImg = JSON.parse(res.data)['fileURL'];
      } catch (error) {
        newImg = '';
      }
      return newImg;
    })
  });

  let imgs = await Promise.all(imgTransPromise).then((imgList) => {
    return imgList
  });

  return imgs;
}

/**
 * 生成一个随机值，并且与有效值进行对比，随机值小于等于有效值上限的，则返回 true，否则返回 false
 * @param {number} effectiveUpper 有效值的上限
 * @param {number} upper 随机值的上限
 */
export const isInRandomExtent = (effectiveUpper = 3, upper = 100) => {
  const value = lodash.random(1, upper);

  return value <= effectiveUpper;
};

/**
 * 结算当前时间离下一周周一中午 12 点半，还有多少秒
 */
export const getWeekEndSeconds = (endAdd = 12) => {
  let now = beijingTime();
  let day = now.day();
  let result = 0;

  if (day === 0) {
    // 周日，直接拿当前时间和下周一早上 12 点（周日凌晨加 12 个小时）的时间作对比
    result = dayjs(now.endOf('day').add(endAdd, 'hour')).diff(now, 'seconds');
  } else if (day === 1) {
    // 周一，如是中午 12 点前，计算当前时间与中午 12 点的差距；
    // 如果是中午 12 点后，计算当前时间与下周周一 12点的差距（本周周末+24 小时+12 小时）
    if (now.hour() < 12) {
      result = dayjs(now.hour(12).minute(0).second(0)).diff(now, 'seconds');
    } else {
      result = dayjs(now.endOf('week').add(24 + endAdd, 'hour')).diff(now, 'seconds');
    }
  } else {
    result = dayjs(now.endOf('week').add(24 + endAdd, 'hour')).diff(now, 'seconds');
  }

  return result;
};

/**
 * 结算当前时间离下个月 1 号中午 12 点半，还有多少秒
 */
export const getMonthEndSeconds = (endAdd = 12) => {
  let now = beijingTime();
  let date = now.date();
  let result = 0;

  if (date === 1) {
    // 1号，如是中午 12 点前，计算当前时间与中午 12 点的差距；
    // 如果是中午 12 点后，计算当前时间与下个月 1 号 12点的差距（月末+12 小时）
    if (now.hour() < 12) {
      result = dayjs(now.hour(12).minute(0).second(0)).diff(now, 'seconds');
    } else {
      result = dayjs(now.endOf('month').add(endAdd, 'hour')).diff(now, 'seconds');
    }
  } else {
    result = dayjs(now.endOf('month').add(endAdd, 'hour')).diff(now, 'seconds');
  }

  return result;
};

const findUsableAd = (adList) => {
  let temp = null;

  temp = lodash.find(adList, (item) => {
    const {
      marketingAdPlanningId
    } = item;

    return !cache.get(`isHadLookAdvert_${marketingAdPlanningId}`);
  });

  return temp;
}

const getRandomJumpInfo = (advert) => {
  const {
    jumpInfoList = [],
  } = advert;
  let length = jumpInfoList.length || 0;
  
  if (!length) {
    return {};
  }

  if (length === 1) {
    return {
      ...advert,
      ...jumpInfoList[0],
    };
  }

  const index = lodash.random(0, length - 1); // jumpInfoList 的随机下标（当 jumpInfoList 数组数量大于 1 时使用）

  console.log('广告链接信息随机下标：', index);
  console.log('广告链接信息随机下标：', jumpInfoList[index]);

  return {
    ...advert,
    ...jumpInfoList[index],
  };
}

/**
 * 获取今天展示给用户的广告，用户每看一个广告，都会在缓存中记录，找到广告列表中还没看过的广告；如果都看过，则重新开始
 * @param {Array} adList 广告列表
 */
export const getMarketingAd = (adList) => {
  // 防止传 null 或者 undefined 进来
  if (!adList) {
    adList = [];
  }

  const length = adList.length;
  let temp = null;

  // 如果数组里面没有值，直接返回一个空对象
  if (!length) {
    return {};
  }

  temp = findUsableAd(adList);

  if (!temp) {
    const isHadPV = lodash.find(adList, (item) => {
      return (item.priceCalType === 'PV') || (item.priceCalType === 'PV_RETENTION');
    });

    if (!isHadPV) {
      // 如果所有广告都已经看过，并且没有 PV 的话，则重置改广告位下的所有广告
      adList.forEach((item) => {
        cache.set(`isHadLookAdvert_${item.marketingAdPlanningId}`, false, getNowAndTargetTimeDuration(1, 'asMilliseconds'));
      });

      return getRandomJumpInfo(adList[0]);
    } else {
      // 如果有 PV 广告，只重置 PV 的广告，UV 不重置
      adList.forEach((item) => {
        if ((item.priceCalType === 'PV') || (item.priceCalType === 'PV_RETENTION')) {
          cache.set(`isHadLookAdvert_${item.marketingAdPlanningId}`, false, getNowAndTargetTimeDuration(1, 'asMilliseconds'));
        }
      });

      temp = findUsableAd(adList);
    }
  }

  console.log('temptemptemp', temp);

  return getRandomJumpInfo(temp ? temp : {});
};

/**
 * 将毫秒转化为时分秒
 * @param {number} value 毫秒数，ps：不是时间戳
 */
export const formatMilliseconds = (value) => {
  let second = parseInt(value) / 1000; // second
  let minute = 0; // minute
  let hour = 0; // hour
  let result;

  if (second > 60) {
    minute = parseInt(second / 60);
    second = parseInt(second % 60);
    if (minute > 60) {
      hour = parseInt(minute / 60);
      minute = parseInt(minute % 60);
    }
  }

  if (second >= 10) {
    result = "" + parseInt(second);
  } else {
    result = "" + "0" + parseInt(second);
  }

  if (minute >= 10) {
    result = "" + parseInt(minute) + ":" + result;
  } else {
    result = "" + "0" + parseInt(minute) + ":" + result;
  }

  if (hour >= 10) {
    result = "" + parseInt(hour) + ":" + result;
  } else {
    result = "" + "0" + parseInt(hour) + ":" + result;
  }
  
  return result;
}
