/*
 * @Author: your name
 * @Date: 2020-03-05 10:41:02
 * @LastEditTime: 2021-05-21 15:06:09
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /taro-kit-v2.0.5/src/utils/appConfig.js
 */


/* 程序调用接口的前缀 */
// 改成从服务器获取配置文件，从中读取真正的请求域名
export const apiPrefix = {
  'development': 'http://apidoc.wechatify.net', // 测试
  'production': '', // 正式
}[process.env.NODE_ENV];


/* cacheTTL失效时间，14天 */
export const cacheExpiredTime = 24 * 60 * 60 * 1000 * 14;

/* RSA公钥 */
export const PUK = `-----BEGIN PUBLIC KEY-----
XXXXXXXXXXXXXXX
-----END PUBLIC KEY-----`;
