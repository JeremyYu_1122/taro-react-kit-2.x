/**
 * 将目标对象转换成formData
 * @param {Object} tarObj 要转成formData的对象
 */
export default function toFormData(tarObj) {
    let formData = new FormData(),
        key = '',
        objKeys = Object.keys(tarObj);

    for (key of objKeys) {
        formData.append(key, tarObj[key]);
    }

    return formData;
};