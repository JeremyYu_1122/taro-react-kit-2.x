 /**
  * 获取场景里的路径参数
  * 扫码进入的小程序页面路径，最大长度 128 字节，不能为空；对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"，即可在 wx.getLaunchOptionsSync 接口中的 query 参数获取到 {foo:"bar"}。
  * @param {*} name 
  * @param {*} search 
  */
export const getQueryStringByScene = (name, search) => {
    return search.indexOf('?') != -1 ?  getQueryString(name,  search) : getQueryString(name, '?' + search)
}

/**
 * 获取query的params
 * @param {*} name 
 * @param {*} search 
 */
export const getQueryString = (name, search = window.location.href) => {
    let reg = new RegExp('(^|&|\\?)' + name + '=([^&]*)(&|$)');

    let r = search.substr(1).match(reg);

    if (r !== null) {
        return unescape(r[2]);
    } else {
        return null;
    }
};
