import { JSEncrypt } from 'encryptlong';

import * as appConfig from '../appConfig';

/**
 * 前端RSA加密
 * 如果加密字符串过长，自动采用分块加密
 * @param {String} data 待加密的数据
 * @param {String} key PUK
 */
const encryptData = (data, key = appConfig.PUK) => {
  let encryptor = new JSEncrypt();

  encryptor.setPublicKey(key);

  // 前端RSA加密，加密字符串过长，提示“Message too long for RSA”问题
  // 所以换成了 JSEncrypt，不过不能直接用，找了一个封装的库—— encryptlong
  let cptData = encryptor.encryptLong(data);

  return cptData;
}

export default encryptData;
