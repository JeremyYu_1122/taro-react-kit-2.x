import Taro from "@tarojs/taro";
import * as appConfig from '@/utils/appConfig';

export const set = (key, value, ttl = appConfig.cacheExpiredTime) => {
    Taro.setStorageSync(
        key,
        JSON.stringify({
            data: value,
            expiredTime: new Date().getTime() + ttl,
        })
    );
};

export const get = (key) => {
    const getItem = Taro.getStorageSync(key);

    try {
        if (getItem && JSON.parse(getItem) && JSON.parse(getItem).expiredTime > new Date().getTime()) {
            return JSON.parse(getItem).data || '';
        } else {
            // return false;
            return ''; //* 返回空字符串感觉更好用 */
        }
    } catch (e) {
        return '';
    }
};
