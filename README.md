
# 跨平台小程序框架
[TOC]

## 环境 & 工具
1. 运行环境：nodejs v12.13.0
1. taro 版本：v2.0.5
1. npm 包安装工作：[tyarn](https://github.com/yiminghe/tyarn)


## 规范
### 命令
1. 安装依赖包：`yarn`

### 目录规范
```
|____taroUiTheme
| |____customTheme.scss      # 自定义 taro-ui 的主题，把所有重写 taro-ui 组件的样式都写在这个文件下
|____cssLib                   # 我们自己的 css 库
|____utils
| |____request.js             # 适配我们请求方式的 request
| |____appConfig.js           # 项目的配置文件
| |____toolbox                # 通用工具库，把 CMS 通用的工具函数都单独抽象成文件放在这个文件夹中，方便以后复用的迁移
| |____dva.js                 # dva 注册文件
|____models                   # dva 的models
|____components               # 项目组件，把所有项目特有的非通用组件都写在这个目录下
| |____components
|____assets                   # 图片等资源文件
|____pages 
|____services                 # dva 的 services
|____index.html
|____app.less
|____app.jsx
```

### 使用说明
#### 模板页面（src/pages/template/template.jsx）
##### 初始化
1. 引用说明，文件头部分别标记了“第三方库”、“自定义组件”、“自定义工具库、工具函数”、“样式”的引入位置，请严格按照既定位置书写
1. 函数的定义不要随便插入，config、state、constructor、生命周期函数、页面事件处理函数、自定义函数，依次书写
1. 修改名称 `Template`
    - 全局匹配（注意要使用“区分大小写”和“全字匹配”），关键字：`Template`，并且替换成新页面的名字
1. 修改标题，修改 config 中的 navigationBarTitleText 字段
1. 检查是否需要下拉刷新，如果需要，开启 config 中的 enablePullDownRefresh 字段，以及 onPullDownRefresh 函数
1. 检查是否需要分享，如果需要，开启 onShareAppMessage 函数，并且检查 appConfig.defaultShareConfig（也可以自定义分享内容）
1. 获取初始化数据
    - 如果该页面需要获取初始化数据的话，将全部接口的调用放在 fetchInitData 函数中，并且对于并发的接口请求，统一用 Promise.all 处理。如果当前页面的接口需要授权登录，则在 Promise.all 的 catch 中开启两种登录方式的一种（静默授权或主动授权，根据业务需要选择一种）
    - 设置好 fetchInitData 函数后，将其放入 componentDidMount 函数中调用（如果需要下拉刷新，在 onPullDownRefresh 函数中也需要再调用一遍）
1. 初始化完之后，记得把页面的 TODO: 都去掉，这些 TODO: 知识为了方便使用 template.jsx 没必要每个页面都留着

##### loading
页面上的 loading 统一使用 dva-loading 控制，并且使用 AtToast 组件进行展示

loading 分两种：
1. page-loading
    - 即：表示该 loading 占满全屏，只有 loading 完才能点击页面
    - 这种 loading 统一使用 this.props.isPageLoading 变量进行管理
    - 如果某个异步请求需要用到 page-loading 的话，则统一在 isPageLoading 后面追加 loading.effects['xxx/xxx]
1. local-loading
    - 即：表示该 loading 只是需要局部展示，不需要占满全屏
    - 这种 loading 根据业务需要自定义变量放在 @connect 中，根据业务需要选择展示的 UI 组件或方式

##### 变量定义规范
定义遵循先定义后使用的原则

1. `state` 变量
    - 对于页面的 state 变量，要么在 `state = {}` 中定义，要么在 `constructor` 中定义，并且赋默认值
1. `this` 变量
    - 统一都在 `constructor` 中定义并且赋默认值
#### models的使用
在 `src/models` 目录下添加自定义 model 后，要在 `src/models/index.js` 文件中添加引用，将 model 注册到 dva 中，不然没法使用

#### events  事件订阅机制
[events](https://github.com/Gozala/events#readme)


```javascript
import EventEmitter from 'events';

constructor(props) {
    super(props);
    this.emitter = new EventEmitter();
}

this.emitter.emit('handleChangeTxt');

<Board emitter={this.emitter} />

在 Board 组件中：
componentDidMount() {
    this.props.emitter && this.props.emitter.on && this.props.emitter.on('handleChangeTxt', this.handleChangeTxt);
}
```

需要注意的是，组件中的 emitter 需要是父组件传递进来的值，不然不是同一个的话，没法绑定在一起