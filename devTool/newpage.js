/**
 * 新增模板页面
 * 接受文件名称
 * 创建 pages、service、model模块
 *  */ 


const inquirer = require('inquirer');
const fs = require('fs');
const fsRead = require('fs');
const path = require('path');

let currentPath = path.resolve(__dirname, '');
let rootPath = path.resolve(__dirname, '..'); //代码文件的根路径

// 每个模块的模板路径
const templateRoute = {
    page: rootPath + '/src/pages/template',
    pageJsx: rootPath + '/src/pages/template/template.jsx',
    pageCss: rootPath + '/src/pages/template/template.less',
    pageConfig: rootPath + '/src/app.jsx',
    services: rootPath + '/src/services/template.js',
    models: rootPath + '/src/models/template.js',
    modelsConfig: rootPath + '/src/models/index.js',
};


// query page name
const questions = [{
    type: 'input',
    name: 'fileName',
    message: 'Please enter file name: ' // 即刚刚生成的swagger文件
  }];
inquirer.prompt(questions).then((answers) => {
    let { fileName } = answers;
    if (!fileName) {
        console.log('文件名称有误')
    } else {
        createFile(fileName);
    }
});


const createFile = (fileName) => {
    // createPage(fileName);
    createService(fileName);
    createModel(fileName);
}


// copy page
    // add route to app.jsx


// copy service
function createService (moduleName) {
    let pathStr = templateRoute.services.replace('template', moduleName);;
    fs.copyFileSync(templateRoute.services, pathStr);
  
    readAndWriteFile({
      pathStr,
      updateFileData (data) {
        let fileData = data;
        fileData = fileData.replace(/template/g, moduleName);
        return fileData;
      }
    });
  }


// copy model
function createModel (moduleName) {
    let pathStr = templateRoute.models.replace('template', moduleName);
    fs.copyFileSync(templateRoute.models, pathStr);
  
    readAndWriteFile({
      pathStr,
      updateFileData (data) {
        let fileData = data;
        fileData = fileData.replace(/template/g, moduleName);
        addModelToIndex(moduleName);
        return fileData;
      }
    });
  }
  
// import model to index.js
function addModelToIndex (moduleName) {
    const addReg = /\/\* add model \*\//;
    const importReg = /\/\* import model page \*\//;

    readAndWriteFile({
        pathStr: templateRoute.modelsConfig,
        updateFileData (data) {
          let fileData = data;
          if (fileData.indexOf(`${moduleName}'`) === -1) { // 排查是否已经有存在该路径数据
              let writeData = moduleName;
              fileData = fileData.replace(addReg, `\t${writeData},\n/* add model */`);

              const importData = `import ${moduleName} from './${moduleName}'`;
              fileData = fileData.replace(importReg, `${importData};\n/* import model page */`);
          }
          return fileData;
        }
      });

}






/*
 * 读写文件 
 * @params obj Object 参数对象
 * @params obj.pathStr 路径名称
 * @params obj.updateFileData 更新数据的函数，将读取的数据传给 updateFileData ，返回更新替换的文件数据流，再重新写入文件中
 * 
 */
function readAndWriteFile (obj) {
    let {
      pathStr = '',
      updateFileData = null
    } = obj;
    console.log('readAndWriteFile', pathStr)
  
    fs.readFile(pathStr, { encoding: 'utf-8' }, (err, data) => {
      if (err) {
        console.log('------- err ------');
        console.log(err);
      }
  
      let fileData = updateFileData ? updateFileData(data) : '';
  
      fs.writeFile(pathStr, fileData, (err2) => {
        if (err2) {
          console.log('------- err2 ------');
          console.log(err2);
        }
      });
    });
  }
